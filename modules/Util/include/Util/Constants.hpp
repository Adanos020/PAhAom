#pragma once


#include <Util/Types.hpp>


namespace util
{

static constexpr unsigned int FPS = 60;
static constexpr DeltaTime FRAME_TIME = 1.0 / FPS;

}