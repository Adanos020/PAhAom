-- Resources to load. Each entry is a table consisting of two auto-indexed
-- strings: resource ID, and path to it.
Resources = {
    -- All paths originate in "data/fonts/".
    fonts = {
        { "unifont", "unifont.ttf" },
    },
    -- All paths originate in "data/textures/".
    textures = {
        { "tileset", "tileset.png" },
    },
    -- All paths originate in "data/music/".
    music = {

    },
    -- All paths originate in "data/sounds/".
    sounds = {

    },
    -- All paths originate in "data/languages/".
    languages = {

    },
}