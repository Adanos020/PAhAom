-- Scripts to load. All paths originate in "data/scripts/".
scripts = {
    -- Do not change these:
    "aux.lua",
    "input.lua",
    "settings.lua",
    "resources.lua",

    -- Below here add your own scripts.
    "GameState/Menu.lua",
    "GameState/Game.lua",
    "Level/dungeon_generator.lua",
}